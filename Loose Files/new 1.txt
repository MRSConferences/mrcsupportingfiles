Bug Fixes and Enhancements up to 5-17-2017

1. https://www.metastasis-research.org/about-us-pages/open-metastasis-related-funding-calls changed into member access only. Changed so that all current members will have access to the page.

2. Changed all the messages, related to role granted, role going to expire and role expired email templates.

3. Fixed html tags in the role granted email from ubercart rule.

